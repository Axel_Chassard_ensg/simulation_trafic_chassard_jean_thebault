﻿# Document utilisateur projet SimulationRoutiere

# Téléchargement du fichier

Télécharger le fichier compressé SimulationRoutiere.tar.xz présent dans le dépôt de git, le clone "https://gitlab.com/Axel_Chassard_ensg/simulation_trafic_chassard_jean_thebault.git"


# Ouverture du fichier

Une fois téléchargé, décompressez-le dans un dossier à part. Ouvrez eclipse, allez dans -> File -> Open Projects From File. Dans la boîte de dialogue qui s'ouvre, cliquez sur "Directory", placez-vous à l'endroit où vous avez décompressé le fichier, sélectionnez le projet et cliquez sur "ok" puis sur "Finish". 

![enter image description here](https://lh3.googleusercontent.com/AcKbEnPDjSP48w3XpAMhB72QmWbZdIEGKsdqx4xYFE2ygGjhjzyBqP0M-OSRU-oa_3j1FaYPTRo)
![enter image description here](https://lh3.googleusercontent.com/wlHizYKp2RQ-O9yOYC97ok15uclq-96pAnax5m27DMDGeM_5a1Z-OOh88asx9X3FLerz5IUR_5s)



# Import de SQLite3

Pour pouvoir tester le projet, il vous est nécessaire d'ajouter SQLite3 au projet eclipse. Pour cela, faites un clic droit sur "SimulationRoutiere" dans l'arborescence de fichier situé dans la partie gauche de la fenêtre eclipse, sélectionnez -> Build Path -> Configure Build Path. Placez-vous dans l'onglet "Librairies". Cliquez sur "Add External JARs", puis sélectionnez l'archive "sqlite-jdbc-3.27.2.1.jar" situé au chemin "SimulationRoutiere/librairies". Cliquez sur "Apply and close"


![enter image description here](https://lh3.googleusercontent.com/BrWBeFZbtQx0x4u41z4amHDzL209danbCQDqiJtIU3YBqvi6wFtFHWYIjMwSb-tK-MSyOrJ9ZIQ)![enter image description here](https://lh3.googleusercontent.com/K2by2IzcHl4vvCtCHG-6RhD5-55kETdPm5TsAU9q9oIEu7MTI3B85i4fYZKkbZsFhyWFpyYRWgA)
![enter image description here](https://lh3.googleusercontent.com/gdypwQzGN6KZ3mx-inBZ0hmyguWO-CflQuJbsagG8BZY_fAaU3p98KvgNjQ687u0aEaRrwFur9Q)


# Lancement de la simulation

Pour lancer la simulation, placez-vous dans la classe "Run". Renseignez dans les lignes 190, 191 et 192 le nombre de véhicules, de piétons et de tours que vous voulez réaliser. Remplacer les chaînes de caractère "identifiantv" ligne 197 et "identifiantp" ligne 203 par les noms que vous souhaitez donner à vos véhicules et vos piétons. Les identifiants des véhicules doivent être différents de ceux des piétons. 

![enter image description here](https://lh3.googleusercontent.com/tj2ZU4VqT5VzgvLs2i_6WKUh8QpWal4WKKHDPKfpaMcsKP5fDLJJohD5aWMPldciXMhw9T8r7Tw)
Vous pouvez désormais lancer la simulation en appuyant sur f11.

Pour obtenir les statistiques de congestion ou le rythme des feux, il vous faut attendre que tous les tours de la simulation soit finis. 

Si vous souhaitez relancer la simulation, veuillez changer les identifiants des figures car il s'agit de la clef primaire de la base de données. 

Il est possible que la situation génère un embouteillage dans des case peut probable. En effet, il est possible que nous n'ayons pas pu tester toutes les configurations possibles. Si les figures se bloquent au carrefour, veuillez relancer la simulation. 




